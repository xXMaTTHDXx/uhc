package me.ocga.ocuhc;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Matt on 02/11/14.
 */
public class TimeManager implements Runnable {

    private static UHC plugin = UHC.plugin;

    private final int GAME_TIME = 2700;
    private final int LOBBY_TIME = 120;
    private final int RESTART_TIME = 120;
    private final int SAFE_PERIOD = 900;
    private final int DEATHMATCH = 900;

    private final int MIN_PLAYERS = 8;

    public void run() {
        if (plugin.getTicks() > 0) {
            plugin.setTicks(plugin.getTicks() - 1);
        }

        if (plugin.getState() == GameState.LOBBY) {
            if (plugin.getTicks() == 10) {
                Bukkit.broadcastMessage(plugin.getPrefix() + "Game starting in " + ChatColor.RED + plugin.getTicks());
            } else if (plugin.getTicks() > 0 && plugin.getTicks() <= 5) {
                Bukkit.broadcastMessage(plugin.getPrefix() + ChatColor.RED + plugin.getTicks());
            } else if (plugin.getTicks() == 0) {
                int online = Bukkit.getOnlinePlayers().length;

                if (online >= MIN_PLAYERS) {

                    Bukkit.getWorld("world").setBiome(plugin.getC1().getBlockX(), plugin.getC2().getBlockY(), Biome.FOREST);

                    for(String key : plugin.getTeams().keySet()){
                        List<UUID> team = plugin.getTeams().get(key);

                        Random random = new Random();

                        Location randomLocation = new Location(Bukkit.getWorld("world"), random.nextDouble(),0,random.nextDouble());

                        if(plugin.isInBetween(randomLocation, plugin.getC1(), plugin.getC2())){
                            randomLocation = new Location(Bukkit.getWorld("world"), random.nextDouble(),0,random.nextDouble());
                        }
                        if(randomLocation.getBlock().getType() == Material.WATER){
                            randomLocation = new Location(Bukkit.getWorld("world"), random.nextDouble(),0,random.nextDouble());
                        }

                        for(UUID uuid : team){
                            Player pl = Bukkit.getPlayer(uuid);
                            pl.teleport(randomLocation);
                        }
                    }

                    Bukkit.broadcastMessage(plugin.getPrefix() + "The game has begun!");
                    plugin.setState(GameState.IN_GAME);
                    plugin.setTicks(SAFE_PERIOD);
                }
                else {
                    plugin.setTicks(LOBBY_TIME);
                    Bukkit.broadcastMessage(plugin.getPrefix() + "We need at least 8 players to start the game!");


                }
            }
        }
        else if(plugin.getState() == GameState.IN_GAME){

            if(plugin.getTicks() % 300 == 0){
                Bukkit.broadcastMessage(plugin.getPrefix() + "The game will end in " + ChatColor.RED + plugin.getTicks() + " seconds");
            }
            else if(plugin.getTicks() > 0 && plugin.getTicks() <= 5){
                Bukkit.broadcastMessage(plugin.getPrefix() + "Game ending in " + ChatColor.GREEN + plugin.getTicks());
            }
            else if(plugin.getTicks() == 0){

                if(Bukkit.getOnlinePlayers().length > 2){
                    for(final Player pl : Bukkit.getOnlinePlayers()){
                        pl.teleport(Bukkit.getWorld("world").getSpawnLocation());
                        pl.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, Integer.MAX_VALUE));

                        Bukkit.broadcastMessage(plugin.getPrefix() + ChatColor.RED + "" + ChatColor.BOLD + "DEATHMATCH!!!!!!");
                        plugin.setTicks(DEATHMATCH);
                        plugin.setState(GameState.DEATHMATCH);

                        new BukkitRunnable(){
                            public void run(){
                                for(PotionEffect potions : pl.getActivePotionEffects()){
                                    pl.removePotionEffect(potions.getType());
                                }
                            }
                        }.runTaskLater(plugin, 25*60);
                    }
                }

                GameManager.getInstance().endGame();
            }
        }
        else if(plugin.getState() == GameState.WARMUP){
            if(plugin.getTicks() % 300 == 0){
                Bukkit.broadcastMessage(plugin.getPrefix() + "PvP will be enabled in " + ChatColor.RED + plugin.getTicks());
            }
            else if(plugin.getTicks() > 0 && plugin.getTicks() <= 5){
                Bukkit.broadcastMessage(plugin.getPrefix() + "Pvp will be enabled in " + ChatColor.GREEN + plugin.getTicks());
            }
            else if(plugin.getTicks() == 0){

                plugin.setState(GameState.IN_GAME);
                plugin.setTicks(GAME_TIME);

                Bukkit.broadcastMessage(plugin.getPrefix() + ChatColor.RED + "" + ChatColor.BOLD + "PvP is now Enabled!");
            }
        }
    }
}
