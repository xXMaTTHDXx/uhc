package me.ocga.ocuhc;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by Matt on 01/11/14.
 */
public class UHC extends JavaPlugin {

    public static UHC plugin;

    private GameState state;

    private String prefix = ChatColor.GRAY + "[" + ChatColor.RED + "OC" + ChatColor.GOLD + "UHC" + ChatColor.GRAY + "] " + ChatColor.GOLD;
    private int ticks = 0;

    private HashMap<String, List<UUID>> teams = new HashMap<String, List<UUID>>();

    private Location midPoint = Bukkit.getWorld("world").getSpawnLocation();

    private Location c1 = new Location(midPoint.getWorld(), midPoint.getBlockX() + 125, 0, midPoint.getBlockZ() + 125);
    private Location c2 = new Location(midPoint.getWorld(), midPoint.getBlockX() - 125, 260, midPoint.getBlockZ() - 125);

    public void onEnable() {
        plugin = this;

        state = GameState.LOBBY;
    }

    public void onDisable() {

    }

    public int getTicks() {
        return ticks;
    }

    public void setTicks(int ticks) {
        this.ticks = ticks;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public String getPrefix() {
        return prefix;
    }

    public HashMap<String, List<UUID>> getTeams() {
        return teams;
    }

    public Location getMidPoint() {
        return midPoint;
    }

    public Location getC1() {
        return c1;
    }

    public Location getC2() {
        return c2;
    }

    public boolean isInBetween(Location loc, Location c1, Location c2) {

        Vector playerV = new Vector(loc.getX(), loc.getY(), loc.getZ());
        Vector min = new Vector(c1.getX(), c1.getY(), c1.getZ());
        Vector max = new Vector(c2.getX(), c2.getY(), c2.getZ());

        return playerV.isInAABB(min, max);
    }

    public Location getLobbyLocation() {
        double x = plugin.getConfig().getDouble("lobby.x");
        double y = plugin.getConfig().getDouble("lobby.y");
        double z = plugin.getConfig().getDouble("lobby.y");
        World world = Bukkit.getWorld(plugin.getConfig().getString("lobby.world"));

        return new Location(world, x, y, z);
    }
}
