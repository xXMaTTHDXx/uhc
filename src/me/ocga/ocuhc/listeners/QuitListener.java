package me.ocga.ocuhc.listeners;

import me.ocga.ocuhc.GameManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Matt on 02/11/14.
 */
public class QuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        if(Bukkit.getOnlinePlayers().length - 1 <= 1){
            GameManager.getInstance().endGame();
        }
    }
}
