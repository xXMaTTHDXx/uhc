package me.ocga.ocuhc.listeners;

import me.ocga.ocuhc.UHC;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Matt on 02/11/14.
 */
public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        if(UHC.plugin.getLobbyLocation() == null){
            e.getPlayer().teleport(new Location(Bukkit.getWorld("world"),0,0,0));
        }

        e.getPlayer().teleport(UHC.plugin.getLobbyLocation());
        Bukkit.broadcastMessage(ChatColor.GRAY + "(" + ChatColor.GREEN + "+" + ChatColor.GRAY + ")");
        Bukkit.broadcastMessage(ChatColor.YELLOW + "" + Bukkit.getOnlinePlayers().length + ChatColor.DARK_AQUA + "/" + ChatColor.YELLOW + Bukkit.getMaxPlayers());
    }
}
