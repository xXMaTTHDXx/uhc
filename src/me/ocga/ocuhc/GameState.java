package me.ocga.ocuhc;

/**
 * Created by Matt on 02/11/14.
 */
public enum GameState {

    LOBBY, IN_GAME, RESTARTING, DISABLED, WARMUP, DEATHMATCH;
}
