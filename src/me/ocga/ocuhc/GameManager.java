package me.ocga.ocuhc;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Created by Matt on 02/11/14.
 */
public class GameManager {

    private static UHC plugin = UHC.plugin;

    private static GameManager instance = new GameManager();

    public static GameManager getInstance() {
        return instance;
    }

    public void endGame() {
        plugin.getTeams().clear();

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);

        String name = plugin.getConfig().getString("LobbyServer");

        for (Player p : Bukkit.getOnlinePlayers()) {
            try {
                out.writeUTF("Connect");
                out.writeUTF(name);
            } catch (IOException ex) {

            }
            p.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
        }

        Bukkit.getServer().unloadWorld("world", true);
        File world = new File("world/");
        world.delete();

        WorldCreator wc = new WorldCreator("world");

        wc.environment(World.Environment.NORMAL);
        wc.createWorld();
    }
}
